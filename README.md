# Bitbucket Security: Secret Scanner

Scans your files for hardcoded secrets, keys, and passwords. Creates a security [Code Insights report][code insights] if found.

This Bitbucket Cloud supported Pipe leverages the [gitleaks][gitleaks] secret pattern registry to provide industry-leading Security Secrets Scanning capabilities to Bitbucket customers.
See Details and Examples section for more information.

**Note**: Breaking changes were introduced in version 2.0.0. Please review the current version of the Pipe and update your configuration accordingly before upgrading.

**Note**: Breaking changes were introduced in version 3.0.0. Please review the current version of the Pipe and update your configuration accordingly before upgrading.


## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/git-secrets-scan:3.1.0
  variables:
    # GITLEAKS_COMMAND: '<string>' # Optional. Default: `git`
    # GITLEAKS_EXTRA_ARGS: '<array>' # Optional.
    # CREATE_REPORT: "<boolean>" # Optional. Default: `true`
    # DEBUG: "<boolean>" # Optional. Default: `false`
```


## Variables

| Variable            | Usage                                                                                                                                                 |
|---------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| GITLEAKS_COMMAND    | Command to use with `gitleaks` Available: `git`, `dir`, `stdin`. Default: `git`.                                                                      |
| GITLEAKS_EXTRA_ARGS | Additional flags to pass to the `gitleaks` [gitleaks extra args][gitleaks extra args].                                                                |
| CREATE_REPORT       | Controls whether the pipe will create Bitbucket [CodeInsights][code insights] reports using the results of the `gitleaks` execution. Default: `true`. |
| DEBUG               | Turn on extra debug information. Default: `false`.                                                                                                    |


## Details

By default, the pipe will scan your files for hardcoded secret credentials with `gitleaks git` command and
create a security report with annotations for each found credential.

### Advanced usage

Take a look at this blogpost for an advanced gitleaks configuration setup: [gitleaks advanced configuration setup].

How to ignore test secrets: [gitleaks additional-configuration].


## Examples

### Basic example:

Run a scan for secrets credentials with default `git` command:

```yaml
script:
  - pipe: atlassian/git-secrets-scan:3.1.0
```

The `dir` command lets you scan directories and files:

```yaml
script:
  - pipe: atlassian/git-secrets-scan:3.1.0
    variables:
      GITLEAKS_COMMAND: "dir"
      GITLEAKS_EXTRA_ARGS: "./path_to_directory_or_file"
```

Do not create a CodeInsights report:

```yaml
script:
  - pipe: atlassian/git-secrets-scan:3.1.0
    variables:
      CREATE_REPORT: "false"
```

### Advanced example:

Scan directories and files, provide custom gitleaks config and turn debug on:

```yaml
script:
  - pipe: atlassian/git-secrets-scan:3.1.0
    variables:
      DEBUG: "true"
      GITLEAKS_COMMAND: "dir"
      GITLEAKS_EXTRA_ARGS: 
        - "./path_to_directory_or_file"
        - "--config=my-gitleaks-config.toml"
```


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=bitbucket-pipelines,pipes,git,secrets,security
[gitleaks]: https://github.com/gitleaks/gitleaks
[code insights]: https://support.atlassian.com/bitbucket-cloud/docs/code-insights/
[gitleaks extra args]: https://github.com/gitleaks/gitleaks?tab=readme-ov-file#usage
[gitleaks additional-configuration]: https://github.com/gitleaks/gitleaks?tab=readme-ov-file#additional-configuration
[gitleaks advanced configuration setup]: https://blog.gitleaks.io/stop-leaking-secrets-configuration-2-3-aeed293b1fbf#15a4