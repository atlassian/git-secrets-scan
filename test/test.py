import os

from bitbucket_pipes_toolkit.test import PipeTestCase


class GitSecretsScanTestCase(PipeTestCase):

    def setUp(self):
        super().setUp()

    def tearDown(self):
        super().tearDown()

    def test_success(self):
        result = self.run_container(environment={
            "CREATE_REPORT": False
        })

        self.assertIn('✔ Pipe has finished successfully.', result)

    def test_gitleaks_allow(self):
        test_discord_client_secret = '8dyfuiRyq=vVc3RRr_edRk-fK__JItpZ'  # gitleaks:allow # noqa: F841
        result = self.run_container(environment={
            "CREATE_REPORT": False
        })

        self.assertIn("✔ Pipe has finished successfully.", result)

    def test_gitleaksignore(self):
        test_discord_client_secret = '8dyfuiRyq=vVc3RRr_edRk-fK__JItpZ'  # noqa: F841

        result = self.run_container(environment={
            "CREATE_REPORT": False,
            "DEBUG": True,
        })

        self.assertIn("✔ Pipe has finished successfully.", result)

    def test_unknown_flag(self):
        result = self.run_container(environment={
            "CREATE_REPORT": False,
            "DEBUG": True,
            "GITLEAKS_EXTRA_ARGS_COUNT": 1,
            "GITLEAKS_EXTRA_ARGS_0": "--unknown"
        })

        self.assertIn('Error: unknown flag', result)


class GitSecretsScanFailsTestCase(PipeTestCase):

    test_failed_secret_filename_json = 'test_failed_secrets.json'

    def setUp(self):
        super().setUp()

        self.cwd = os.getcwd()
        self.test_dir = "test"
        os.chdir(self.test_dir)

        with open(self.test_failed_secret_filename_json, 'w') as f:
            f.write(''.join(['AKIAKEGEYJSDOHQY', 'TEST']))

        os.chdir(self.cwd)

    def tearDown(self):
        super().tearDown()

        os.remove(f"{self.test_dir}/{self.test_failed_secret_filename_json}")

    def test_leaks_found(self):
        result = self.run_container(environment={
            "GITLEAKS_COMMAND": "dir",
            "CREATE_REPORT": False,
            "DEBUG": True,
            "GITLEAKS_EXTRA_ARGS_COUNT": 1,
            "GITLEAKS_EXTRA_ARGS_0": "./test"
        })

        self.assertIn('leaks found', result)
