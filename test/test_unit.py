import logging
import os
import sys
from copy import copy
from unittest import TestCase

import pytest

from pipe.pipe import GitSecretsScanPipe, schema


class TriggerBuildTestCase(TestCase):
    @pytest.fixture(autouse=True)
    def inject_fixtures(self, caplog, capsys, mocker, fp):
        self.caplog = caplog
        self.capsys = capsys
        self.mocker = mocker
        self.fp = fp

    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path

    def test_default_success(self):
        self.mocker.patch.dict(
            os.environ, {
                "CREATE_REPORT": 'false',
                'DEBUG': 'true'
            }
        )

        self.fp.register(
            ["gitleaks", "git", "-v"],
            stdout=["gitleaks finished successfully"]
        )

        pipe = GitSecretsScanPipe(schema=schema, check_for_newer_version=True)

        with self.caplog.at_level(logging.DEBUG):
            pipe.run()

        out = self.capsys.readouterr().out
        self.assertRegex(out, "✔ Pipe has finished successfully.")

    def test_default_fail(self):
        self.mocker.patch.dict(
            os.environ, {
                "CREATE_REPORT": 'false',
                'DEBUG': 'true'
            }
        )

        self.fp.register(
            ["gitleaks", "git", "-v"],
            returncode=1
        )

        pipe = GitSecretsScanPipe(schema=schema, check_for_newer_version=True)

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            pipe.run()

        self.assertEqual(pytest_wrapped_e.type, SystemExit)

        out = self.capsys.readouterr().out
        self.assertRegex(out, "✖ Pipe has finished with a Results Status Code: 1")

    def test_default_fail_another_command(self):
        self.mocker.patch.dict(
            os.environ, {
                "GITLEAKS_COMMAND": 'dir',
                "CREATE_REPORT": 'false',
                'DEBUG': 'true'
            }
        )

        self.fp.register(
            ["gitleaks", "dir", "-v"],
            returncode=1
        )

        pipe = GitSecretsScanPipe(schema=schema, check_for_newer_version=True)

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            pipe.run()

        self.assertEqual(pytest_wrapped_e.type, SystemExit)

        out = self.capsys.readouterr().out
        self.assertRegex(out, "✖ Pipe has finished with a Results Status Code: 1")

    def test_fail_wrong_command(self):
        self.mocker.patch.dict(
            os.environ, {
                "GITLEAKS_COMMAND": 'fail',
                "CREATE_REPORT": 'false',
                'DEBUG': 'true'
            }
        )

        with pytest.raises(SystemExit) as pytest_wrapped_e:
            GitSecretsScanPipe(schema=schema, check_for_newer_version=True)

        self.assertEqual(pytest_wrapped_e.type, SystemExit)

        out = self.capsys.readouterr().out
        self.assertRegex(out, "Validation errors: \nGITLEAKS_COMMAND:\n- unallowed value fail\n")
