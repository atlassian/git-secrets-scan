FROM zricethezav/gitleaks:v8.23.3

# Install python
ENV PYTHONUNBUFFERED=1
RUN apk update --no-cache && \
    apk add --update --no-cache python3~=3.11 py3-pip~=23

# Install requirements
COPY requirements.txt /
WORKDIR /
RUN pip install --break-system-packages --no-cache-dir -r requirements.txt


# Copy the pipe source code
COPY pipe /
COPY pipe.yml LICENSE.txt README.md /

ENTRYPOINT ["python3", "/pipe.py"]
