# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 3.1.0

- minor: Add commit to file path in annotations.
- patch: Internal maintenance: Update docker image zricethezav/gitleaks to v8.23.3.
- patch: Internal maintenance: bump pipes versions in pipelines config file.

## 3.0.0

- major: Update gitleaks version. Replace deprecated commands. Breaking change! You have to update your pipe variables. Take a look at the README for an additional information.

## 2.0.3

- patch: Internal maintenance: Update description in pipe.yml, README.

## 2.0.2

- patch: Internal maintenance: Update README.

## 2.0.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 2.0.0

- major: Replace secrets scanning tool to gitleaks. Breaking change! You have to update your pipe variables. Take a look at the README for an additional information.
- patch: Internal maintenance: bump packages in requirements.
- patch: Internal maintenance: bump pipes versions in pipelines config file.

## 1.5.1

- patch: Internal maintenance: Update pipe name.
- patch: Internal maintenance: Update pipes versions in pipelines config file.

## 1.5.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 1.4.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 1.3.0

- minor: Update bitbucket-pipes-toolkit to fix vulnerability with certify.

## 1.2.1

- patch: Internal maintenance: fix passing list type parameters in tests.

## 1.2.0

- minor: Added support for FILES_IGNORED variable. Now pipe supports files to be ignored during secrets scanning (glob patterns supported).

## 1.1.1

- patch: Add a lower bound for the version of setuptools to fix security issues.

## 1.1.0

- minor: Reduce pipe size by update docker image to python:3.10-slim

## 1.0.2

- patch: Add GCP pattern example.

## 1.0.1

- patch: Internal maintenance: pipelines config update images to atlassian/default-image:3 and python:3.10.
- patch: Internal maintenance: update Dockerfile to fix hadolint.
- patch: Internal maintenance: update community link.
- patch: Internal maintenance: update docker image to python:3.10.
- patch: Internal maintenance: update release process.
- patch: Internal maintenance: update test/requirements to fix security issues.

## 1.0.0

- major: Add feature to support multiple custom patterns.

## 0.6.1

- patch: Internal maintenance: Bump version of bitbucket_pipes_toolkit to 3.2.1

## 0.6.0

- minor: Refactor documentation in README

## 0.5.1

- patch: Internal maintenance: add bitbucket-pipe-release.

## 0.5.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 0.4.3

- patch: Fix wildcard usage in FILES variable.

## 0.4.2

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.4.1

- patch: Internal maintenance: Update pipelines.

## 0.4.0

- minor: Add support for CUSTOM_PATTERN variable.

## 0.3.0

- minor: Added new FILES parameter

## 0.2.0

- minor: Add support for authless API requests

## 0.1.0

- minor: Initial release
